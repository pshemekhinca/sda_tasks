import random
# ***** exc_01 *****

# CURRENT_YEAR = 2021
# birth_year = int(input("Podaj rok urodzenia: "))
# age = CURRENT_YEAR - birth_year
# print("You are adult") if age >= 19 else print("Too young")

# ***** exc_02 *****

# day_number =int(input('Podaj nr dnia w tygodniu: '))
# week_days = ['Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek', 'Sobota', 'Niedziela']
# print(day_number, '-', week_days[day_number - 1])

# ***** exc_03 *****
#
# first_nb = int(input('Podaj pierwszą liczbę: '))
# second_nb = int(input('Podaj drugą liczbę: '))
# third_nb = int(input('Podaj trzecią liczbę: '))
# user_list = [first_nb, second_nb, third_nb]

# if len(set(user_list)) == 1:
#     print('Podane liczby są takie same.')
# elif len(set(user_list)) == 2:
#     print('Dwie z podanych liczb są takie same.')
# else:
#     print('Podano trzy różne liczby')

# ***** exc_04 *****

# current_year = int(input('Podaj obecny rok: '))
# current_month = int(input('Podaj który mamy miesiąc: '))
# add_months = int(input('Ile m-cy chcesz dodać: '))
#
# new_year = int(current_year + (current_month + add_months) / 12)
# new_month = (current_month + add_months) % 12
# if new_month == 0:
#     print(f'Po dodaniu {add_months} m-cy będzie {new_month + 12} m-c {new_year - 1} roku')
# else:
#     print(f'Po dodaniu {add_months} m-cy będzie {new_month} m-c {new_year} roku')

# ***** exc_05 *****

# user_number = input('Podaj wielocyfrową liczbę całkowitą: ')
# for digit in user_number:
#     print(digit)

# ***** exc_06 *****

# numb = 0
# sum_numb = 0
# turns = 0
# while sum_numb <= 100:
#     numb = int(input("Podaj liczby całkowite (sumujemy do 100): "))
#     sum_numb = sum_numb + numb
#     turns = turns + 1
#     if sum_numb > 100:
#         over_100 = sum_numb - 100
#         print(f'\nOstatnia liczba {numb} przekroczyła 100 o {over_100} punktów.')
#         break
#     print(f'Aktualna suma = {sum_numb}')
# print(f"Wprowadzonych liczb: {turns}\nSuma wprowadzonych liczb to {sum_numb}")

# ***** exc_07 *****

# TODO:

# ***** exc_08 *****

# user_number = int(input('Podaj liczbę naturalną: '))
# numbers = list(range(1, user_number + 1))
# print(f'Suma liczb od [1] do [{user_number}] wynosi {sum(numbers)}')

# ***** exc_09 *****
#
# l3 = list(range(100, 1000))
# for list_item in l3:
#     digits = list(str(list_item))
#     # item_digits_sum = sum(int(digit) for digit in digits)
#     item_digits_sum = int(digits[0]) + int(digits[1]) + int(digits[2])
#     if item_digits_sum == 3 * (int(digits[0]) * int(digits[1]) * int(digits[2])):
#         print(f'Liczba której suma cyfr = 3 x iloczyn jej cyfr to {list_item}.')

# ***** exc_10 *****

# l_counter = 0
# r_counter = 0
# left_end = 0
# right_end = 0
# print("Jesteś w ciemnym tunelu.")
# while left_end != 3 and right_end != 4:
#     direction = input("W którę stronę chcesz iść? Lewo [l] czy Prawo [p]: ")
#     direction = direction.lower()
#     if direction == 'l':
#         left_end = left_end + 1
#         right_end = right_end - 1
#         l_counter = l_counter + 1
#         print("Poszedłeś o krok w lewo")
#     elif direction == 'p':
#         right_end = right_end + 1
#         left_end = left_end - 1
#         r_counter = r_counter + 1
#         print("Poszedłeś o krok w prawo")
#
# print("\n!!!! @#$ !!!!\nŻegnaaaaaaajjjjj....")
# print("Wykonano", l_counter, "kroków w lewą stronę\noraz", r_counter, "kroków w prawo")

# ***** exc_11 *****

# 11A z wykorzystaniem listy

# user_string = (input('Podaj ciąg znaków składający sie wyłącznie z liter: ')).lower()
# if user_string.isalpha():
#     string_to_list = list(user_string)
#     string_to_set = set(string_to_list)
#     for i in string_to_set:
#         print(f"'{i}' występuje {string_to_list.count(i)}-krotnie")
# else:
#     print('Oprócz liter wprowadzono też inne znaki')
#
# print('********')

# user_string = (input('Podaj ciąg znaków składający sie wyłącznie z liter: ')).lower()
# if user_string.isalpha():
#     string_to_list = list(user_string)
#     distinct_list = []
#     for i in range(len(string_to_list)):
#         for x in range(i + 1, len(string_to_list)):
#             if string_to_list[i] == string_to_list[x] and string_to_list[i] not in distinct_list:
#                 distinct_list.append(string_to_list[i])
#     for i in distinct_list:
#         print(f"'{i}' występuje {string_to_list.count(i)}-krotnie")
# else:
#     print('Oprócz liter wprowadzono też inne znaki')

# ***** exc_12 *****

# user_amount = int(input('Podaj ile liczb wylosować: '))
# user_shift = int(input('Podaj o ile miejsc przesunąć elementy listy: '))
#
# rnd_array = list(random.sample(range(0, 100), user_amount))
# print(f'\nLista początkowa\n{rnd_array}')
# new_array = rnd_array.copy()
#
# if user_shift % len(new_array) == 0:
#     print(f'Przesuniecie o {user_shift} miejsc nie zmienia porządku elementów listy.')
# elif user_shift <= len(new_array):
#     new_array = new_array[-user_shift:] + new_array[:-user_shift]
# else:
#     big_shift = user_shift % len(new_array)
#     new_array = new_array[-big_shift:] + new_array[:-big_shift]
# print(f'\nLista z przesunięciem miejsc o {user_shift} w prawo:\n{new_array}')


# ***** exc_13 *****

# user_string = list(input('Podaj dowolny ciąg znaków: '))
# for item in range(0, len(user_string), 2):
#     temp = user_string[item]
#     user_string[item] = user_string[item + 1]
#     user_string[item + 1] = temp
#
# swaped_string = ''.join(user_string)
# print(swaped_string)


# ***** exc_14 *****
#
# def check_if_6(numbers):
#     while len(numbers) != 6 or len(set(numbers)) < 6:
#             print(numbers)
#             print('\nNie podałeś 6 liczb lub powtórzyłeś jedną z nich.')
#             numbers = input('Podaj 6 różnych liczb z przedziału 1-49 (oddzielone spcją): ').split()
#             print(numbers)
#     return True
#
# def check_num_range(numbers):
#     for i in numbers:
#         while i not in range(1-49):
#             numbers = input('Podaj liczby całkowite nie większe niż 49\nPodaj 6 liczb oddzielonych spacją: ').split()
#         else:
#             print(numbers)
#     return True
#
#
# user_nmbrs = input('Podaj 6 liczb z przedziału 1-49 (oddziel je spcją): ').split()
# while check_if_6(user_nmbrs) != check_if_6(True) and check_num_range(user_nmbrs) != True:
#     print('')
#
# print(f'Twoje liczby {user_nmbrs} są poprawne')

# print(user_nmbrs)
# usr_nmbrs = [2, 3, 4, 5, 6, 7]
# same_list = []
# counter = 0
# three_count = 0
# four_count = 0
# five_count = 0
#
# while len(same_list) != 6:
#     comp_nmbrs = random.sample(range(1, 50), 6)
#     counter += 1
#     print(counter)
#     print(three_count)
#     if usr_nmbrs.sort() == comp_nmbrs.sort():
#         for nmbr in usr_nmbrs:
#             if nmbr in comp_nmbrs or nmbr not in same_list:
#                 same_list.append(nmbr)
#             if len(same_list) == 3:
#                 three_count += 1
#             elif len(same_list) == 4:
#                 four_count += 1
#             elif len(same_list) == 5:
#                 five_count += 1
#             elif len(same_list) == 6:
#                 break
#         same_list.clear()
#     else:
#         print('W końcu! Gratulacje!')
#
# print(f'3 x {three_count}, 4 x {four_count}, 5 x {five_count}')
# print(f' trafiony za {counter} razem')

# ***** exc_15 *****

# user_quant = int(input('Podaj ile liczb wylosować: '))
# user_range = int(input('Od 0 do jakiej liczby wykonać losowanie? Podaj liczbę: '))
# # 15_A
# rnd = random.sample(range(0, user_range), user_quant)
# print(rnd)
#
# # 15_B
# rnd2 = set()
# for i in range(user_quant):
#     # jeżeli liczba się powtarzała to set tworzył krótszą listę niż wskazana przez użytkownika stąd while
#     while len(rnd2) != user_quant:
#         i = random.randint(0, user_range)
#         rnd2.add(i)
# print(rnd2)

# ***** exc_16 *****

# user_number = int(input('Rysujemy N. Podaj liczbę: '))
# for row in range(user_number):
#     for col in range(user_number):
#         if row == 0 or col == 0 or row == col or col == user_number-1 or row == user_number - 1:
#             print('*', end='')
#         else:
#             print(end=' ')
#     print()


# ***** exc_17 *****

# user_size = int(input('Rysujemy szachownicę. Podaj jej wielkość: '))
# lista1 = list(range(1, user_size+1))
#
# for row in range(user_size):
#     if row % 2 == 0:
#         for col in lista1:
#             if col % 2 == 0:
#                 print('1', end='')
#             else:
#                 print('0', end='')
#         print()
#     else:
#         for col in lista1:
#             if col % 2 == 0:
#                 print('0', end='')
#             else:
#                 print('1', end='')
#         print()


# ***** exc_18 *****

# user_number = int(input('Podaj liczbę: '))
# multi_list = []
# suma = 0
# for i in range(1, user_number + 1):
#     multi_list.append(str(i) * i)
# for i in multi_list:
#     suma = int(i) + suma
# print(' + '.join(multi_list), '=', suma)


# ***** exc_19 *****

# list_1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
# list_2 = ['a', 'b', 'c', 'd']
# len_difference = len(list_2) - len(list_1)
# list_1a = list_1[:len_difference]
# mixed = []
# for i in range(len(list_2)):
#     mixed.append(list_1[i])
#     mixed.append(list_2[i])
# print(mixed + list_1[len_difference:])


# ***** exc_20 *****

# user_sentence = input('Podaj ciąg znaków, mix wielkich i małych liter:\n')
# converted = list()
# for i in user_sentence:
#     if i == i.lower():
#         converted.append(i.upper())
#     elif i == i.upper():
#         converted.append(i.lower())
# print(''.join(converted))


# ***** exc_21 *****

# user_sentence = input('Podaj zdanie lub kilka wyrazów oddzielonych spacją:\n')
# print(user_sentence.title())


# ***** exc_22 *****

# messy = [23, 12, 2, 4, 26, 16, 1, 7, 10]
# print(f'bałagan  -> {messy}')
# len_list = len(messy)
# for j in range(len_list):
#     for i in range(0, len_list - 1):
#         if messy[i] > messy[i + 1]:
#             messy[i], messy[i + 1] = messy[i + 1], messy[i]
#     len_list -= 1
# print(f'porządek -> {messy}')


# ***** exc_23 *****

#TODO:

# ***** exc_24 *****

# mixed_data = [234, 3.14, 2020, 'October', 'xyz', 5.25, '3.40 pln', [1, 2, 3], 15.5, None]
# int_list = []
# float_list = []
# varia_list = []
# for i in mixed_data:
#     if type(i) == int:
#         int_list.append(i)
#     if type(i) == float:
#         float_list.append(i)
#     if type(i) != int and type(i) != float:
#         varia_list.append(i)
# print('Lista początkowa', mixed_data)
# print('Lista liczb całkowitych',int_list)
# print('Lista liczb zmiennoprzecinkowych',float_list)
# print('Lista pozostałych elementów',varia_list)
