import random

# # ***** exc_25 *****
#
# def eliminate_doubles(usr_str):
#     """Creates new string without double characters"""
#     new_str = ''
#     usr_str.append('')
#     for i in range(len(usr_str) - 1):
#         letter = usr_str[i]
#         nxt_letter = usr_str[i + 1]
#         if letter != nxt_letter:
#             new_str = new_str + letter
#     print(new_str)
#
#
# usr_str = list(input('Podaj ciąg z podwójnymi znakami: '))
# eliminate_doubles(usr_str)
#
# # ***** exc_26 *****
#
# def sort_strings(str_1, str_2):
#     """Sorts and compares strings"""
#     if sorted(str_1) == sorted(str_2):
#         print('Każde ze słów może być anagramem drugiego.')
#     else:
#         print('Nie można stworzyć anagramu. Sprawdź czy w obu słowach są te same znaki')
# #
# def check_lists(str_n):
#     """Converts list into dictionary
#     and counts how many times elements appears"""
#     temp_dict = {}
#     for i in str_n:
#         temp_dict[i] = str_n.count(i)
#     return temp_dict
#
# # 1st
# str_1 = input('Podaj pierwszy wyraz: ')
# str_2 = input('Podaj dugi wyraz: ')
# sort_strings(str_1, str_2)
#
# # 2nd
# str_1 = list(input('Podaj pierwszy wyraz: '))
# str_2 = list(input('Podaj dugi wyraz: '))
# if check_lists(str_1) == check_lists(str_2):
#     print('Każde ze słów może być anagramem drugiego.')
# else:
#     print('Znaki w obu ciągach są różne, nie można stworzyć anagramu.')
#
# str_2 = list('pelinepl')
# str_2 = list('pelinepl')
#
# # ***** exc_27 *****
#
# def check_differ(inp_1, inp_2):
#     """Return difference of two given lists"""
#     diff = []
#     print(inp_1 + inp_2)
#     for i in inp_1 + inp_2:
#         if i not in inp_1 or i not in inp_2:
#             diff.append(i)
#     return ''.join(diff)
#
# input_1 = input('Podaj 1 ciąg: ')
# input_2 = input('Podaj 2 ciąg: ')
# print(check_differ(input_1, input_2))
#
#
# # ***** exc_28 *****
#
# def count_vowels(user_str):
#     """Counts vowels in given string"""
#     vowels = list('aąeęuóioyAĄEĘUÓIOY')
#     vowel_count = 0
#     for i in user_str:
#         if i in vowels:
#             vowel_count += 1
#     print(f'Ilość samogłosek w podanym ciągu = {vowel_count} .')
#
# user_str = input('Podaj ciąg znaków, policzę samogłoski: ')
# count_vowels(user_str)

#
# # ***** exc_29 *****
#
# def create_different(dict_1, dict_2):
#     """Compares two dicts & create third one of different keys"""
#     dict_1 = set(dict_1.keys())
#     dict_2 = set(dict_2.keys())
#     difference = dict_1 ^ dict_2
#     print(f'Słownik z kluczy nie powtarzajacych się {dict.fromkeys(difference)}')
#
#
# dict_1 = dict.fromkeys('asdfghjkl')
# dict_2 = dict.fromkeys('qwertyghjkl')
# create_different(dict_1, dict_2)
#
# # ***** exc_30 *****


PIK = chr(9824)
KIER = chr(9829)
KARO = chr(9830)
TREFL = chr(9827)
SUITS_LIST = [PIK, KIER, KARO, TREFL]
FACE_CARDS = ['J', 'Q', 'K', 'A']


class Deck:

    def __init__(self, deck):
        self.deck = deck


    def create_deck(self):
        """Creates deck of cards"""
        deck = []
        for suit in SUITS_LIST:
            for rank in range(2, 11):
                card = (suit, rank)
                deck.append(card)
            for rank in FACE_CARDS:
                card = (suit, rank)
                deck.append(card)
        return self.deck


# def deal_cards(players, cards):
#     pass
#
# # players = int(input('Podaj ilu jest graczy: '))
# # cards_qty = int(input('Ile kart rozdać każdemu graczowi: '))
# players= 4
# cards_qty = 5
# deck = Deck('whole deck')
print(Deck.create_deck())
# deck = create_deck()
# rnd = random.randint(1, 52)
# st = deck.pop(rnd)
# print(st)
# print(deck)

#
# # ***** exc_31 *****
#
# def create_idx_list(usr_str, item):
#     """Creates given character indexes list"""
#     idx_list = list(i for i, value in enumerate(usr_str) if value == item)
#     if len(idx_list) >= 1:
#         print(f'Znak [{item}] ma w twoim ciągu następujące indeksy: {idx_list}')
#     else:
#         print(f'Niestety znak [{item}] nie występuje w stringu czyli {idx_list}')
#
#
# usr_str = list(input('Podaj ciąg znaków: '))
# item = input('Podaj znak do zindeksowania: ')
# create_idx_list(usr_str, item)
#
# # ***** exc_32 *****
#
# def create_intesect(list_1, list_2):
#     """Creates intersection values list out of two given lists"""
#     inter_list = [value for value in list_1 if value in list_2]
#     print(f'Elelementy występujące w obu listach to {inter_list}')
#
# list_1 = list('asdfghjkl')
# list_2 = list('qwertylkjhg')
# create_intesect(list_1, list_2)
#
# # ***** exc_33 *****

#TODO: PESEL generator app

# # ***** exc_34 *****


# # ***** exc_35 *****

# def check_qty(int_1, int_2):
#     """Check digits apperances in 2 inputs"""
#     if sorted(int_1) == sorted(int_2):
#         print('W obu liczbach występują te same cyfry w tej samej ilości.')
#     else:
#         print('W obu liczbach występują różne cyfry lub ich różna ilość.')
#
# int_1 = list(int(input('Podaj 1szą liczbę: ')))
# int_2 = list(int(input('Podaj 2gą liczbę')))
# check_qty(int_1, int_2)

# # ***** exc_36 *****

# msg = input("Podaj jakis napis ")
# c = input("Podaj znak do wyszukiwania ")
# c_idx = None
# # pytanie o pierwszy znak w ciągu zwraca jego brak, enumerate powinien zacząć liczyć od 1
# for idx, val in enumerate(msg, 1):
#     if val == c:
#         c_idx = idx
#         break
# if c_idx:
#     print("Pierwszy znak jest na pozycji", c_idx)
# else:
#     print("Nie ma znaku")

# # ***** exc_37 *****
#
# def put_line_number(data):
#     """Creates new_file with line numbers added to each line of a read_file"""
#     line_numb = 1
#     file_to_write = open('/home/peha/Desktop/data_test2.txt', 'w')
#     for the_line in data:
#         file_to_write.writelines(str(line_numb) + " " + the_line)
#         line_numb += 1
#
#
# with open('/home/peha/Desktop/data_test1.txt', 'r') as read_file:
#     data_in = read_file.readlines()
#     put_line_number(data_in)


# # ***** exc_38 *****
# import string
#
# def count_digits(data):
#     """Creates a tuples list with apperance of each letter
#     and returns the most numerous 3 records"""
#     quantity_tuple = []
#     for char in data:
#         temp_data_pair = (char, data.count(char))
#         if char.isalpha() and temp_data_pair not in quantity_tuple:
#             quantity_tuple.append(temp_data_pair)
#     tuple_pair_swap = [(i[1], i[0]) for i in quantity_tuple]
#     top_three = sorted(tuple_pair_swap)
#     return f'Najczęściej występujące litery w tekście to: {top_three[-3:]}'
#
# alpha_en = list(string.ascii_lowercase)
# diacritic = ['ą', 'ć', 'ę', 'ó', 'ł', 'ń', 'ź', 'ż', 'ś']
# alpha_pl = alpha_en + diacritic
#
# with open('/home/peha/Desktop/data_test.txt', 'r') as file_to_read:
#     data = file_to_read.read()
#     print(count_digits(data.lower()))

# # ***** exc_39 *****
#
# def count_words(data):
#     """Counts words without """
#     splitted_data = data.split()
#     quantity_tuple = []
#     for word in splitted_data:
#         temp_data_pair = (word, splitted_data.count(word))
#         if word.isalpha() and temp_data_pair not in quantity_tuple:
#             quantity_tuple.append(temp_data_pair)
#     tuple_pair_swap = [(i[1], i[0]) for i in quantity_tuple]
#     return sorted(tuple_pair_swap, reverse=True)
#
# with open('/home/peha/Desktop/data_test.txt', 'r') as file_in:
#     data = file_in.read()
#     print(count_words(data.lower()))

# # ***** exc_40 *****

# def find_average(data, sex, col):
#     """Counts average of given data"""
#     data_for_avg = []
#     for i in data:
#         p = i.split()
#         if sex == p[3]:
#             data_for_avg.append(int(p[col]))
#     average = sum(data_for_avg) / len(data_for_avg)
#     return average
#
#
# def find_max_min(data, sex, min_max):
#     """Finds min or max value of given data"""
#     data_min_max = []
#     for i in data:
#         p = i.split()
#         if sex == p[3]:
#             data_min_max.append(int(p[1]))
#     return min_max(data_min_max)
#
#
# with open('/home/peha/Desktop/pacjenci_1.txt', 'r') as file_in:
#     bmi_data = file_in.readlines()
#
#     woman_weight = round((find_average(bmi_data, 'K', 2)), 2)
#     woman_height = find_average(bmi_data, 'K', 1)
#     print(f'Grupa kobiet:\n\t- średni wzrost = {woman_height}\n\t- średnia waga = {woman_weight}')
#     man_weight = round((find_average(bmi_data, 'M', 2)), 2)
#     man_height = find_average(bmi_data, 'M', 1)
#     print(f'Grupa mężczyzn:\n\t- średni wzrost = {man_height}\n\t- średnia waga = {man_weight}')
#
#     man_max_min = hight = find_max_min(bmi_data, 'M', max)
#     woman_max_min = find_max_min(bmi_data, 'K', min)
#     print(f'Najwyższy mężczyzna ma wzrost = {man_max_min}')
#     print(f'Najniższa kobieta ma wzrost = {woman_max_min}')

# # ***** exc_41 *****
# # ***** exc_42 *****

# l = list('abcdef')
# for i in range(0, len(l), 2):
#     l[i], l[i + 1] = l[i + 1], l[i]
# print(l)


# # ***** exc_43 *****

# def find_average(data):
#     """Counts average of given data"""
#     data_for_avg = []
#     for i in data:
#         p = i.split(',')
#         data_for_avg.append(float(p[2]))
#     average = sum(data_for_avg) / len(data_for_avg)
#     return round(average, 2)
#
#
# with open('dane.csv', 'r') as file_in:
#     data = file_in.readlines()
#     print(f'\nŚrednia wszystkich liczb z 3 pozycji = {find_average(data)}')
